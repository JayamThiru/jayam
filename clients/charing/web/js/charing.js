$(document).ready(function() {
    "use strict";
    var aSearchClicked = false;
    jQuery(".sub-menu").hide();

    if ("ontouchstart" in document.documentElement)
    {
        jQuery(".menu-item-has-children").bind('touchstart touchon', function(event) {
            event.preventDefault();
            jQuery(this).children(".sub-menu").toggleClass("active").toggle(350);
            return false;
        }).children(".sub-menu").children("li").bind('touchstart touchon', function(event) {
            window.location.href = jQuery(this).children("a").attr("href");
        });
        $('#a-search').bind('touchstart touchon', function(event) {
            if (aSearchClicked)
            {
                $('#searchform').removeClass('moved');
                aSearchClicked = false;
            }
            else
            {
                $('#searchform').addClass('moved');
                aSearchClicked = true;
            }
        });
    }
    else
    {

        jQuery(".menu-item-has-children").bind('click', function(event) {
            event.preventDefault();
            jQuery(this).children(".sub-menu").toggleClass("active").toggle(350);
            return false;
        }).children(".sub-menu").children("li").bind('click', function(event) {
            window.location.href = jQuery(this).children("a").attr("href");
        });
        $('#a-search').bind('click', function(event) {
            if (aSearchClicked)
            {
                $('#searchform').removeClass('moved');
                aSearchClicked = false;
            }
            else
            {
                $('#searchform').addClass('moved');
                aSearchClicked = true;
            }
        });
    }



    $.addTemplateFormatter({
        DateFormat: function(value, template) {
            if (template == "verylong") {
                return $.format.date(value, "ddd, dd/MM/yyyy HH:mm");
            } else if (template == "verylong2") {
                return $.format.date(value, "ddd, dd-MMM-yyyy");
            } else if (template == "long") {
                return $.format.date(value, "dd/MM/yyyy HH:mm");
            } else if (template == "short-") {
                return $.format.date(value, "dd-MMM-yyyy");
            } else {
                return $.format.date(value, "dd/MM/yyyy");
            }
        },
        lovedFormat: function(value, options) {
            if (value == "1") {
                return '<img src="images/heart-icon.png" alt="favorite" />';
            } else {
                return '<img src="images/heart-icon-gray.png" alt="favorite" />';
            }
        },
        lovedAction: function(value, options) {
            if (value == "1") {
                return 'unlove(this)';
            } else {
                return 'love(this)';
            }
        },
        lovedActionFromFav: function(value, options) {
            return 'unloveFromFav(this)';
        },
        lovedId: function(value, options) {
            return "love_" + value;
        },
        eventId: function(value, options) {
            return "event_" + value;
        },
        eventHref: function(value, options) {
            return "#single_event?id=" + value;
        },
        eventTitle: function(value, options) {
            return value;
        },
        favId: function(value, options) {
            return "fav_" + value;
        },
        favHref: function(value, options) {
            //return "#single_fav?id=" + value;
            return "single_favorite(" + value + ")";
        },
        mshopId: function(value, options) {
            return "mshop_" + value;
        },
        mshopHref: function(value, options) {
            return "#single_shop?id=" + value;
        },
        barcodeImg: function(value, options) {
            return "shopbarcode_" + value;
        },
        ticketType: function(value, template) {
            var out = "";
            var data = jQuery.parseJSON(value);
            jQuery.each(data, function(idx, obj) {
                out = out + '<option value="' + obj.value + '">' + obj.name + '</option>';
            });
            return out;
        },
        CharingCoins: function(value, template) {
            return Math.ceil(value);
        },
        CurrencySymbol: function(value, template) {
            if (value == "pound") {
                return '&#xa3;';
            } else if (value == "euro") {
                return '&#x20ac;';
            } else if (value == "doller") {
                return '$';
            } else {
                return value;
            }
        }
    });

//calc
});

/* Login Module
 * --------------------------------------------------------------------
 */

function validateLogin() {
    if (!validateEmail(jQuery("#email").val())) {
        alert("Please enter valid email");
        return false;
    }
    //if (jQuery("#email").val() != jQuery("#remail").val()) {
    //    alert("Confirm email mismatching");
    //    return false;
    //}
    return true;
}

var looping;

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function logout() {
    $.removeCookie('charing_uid');
    $.removeCookie('charing_email');
    $(":mobile-pagecontainer").pagecontainer("change", "index.html", {reload: true});
}

function lockMsg() {
    alert('Your sensitive data including card details are encrypted before transmission. Moreover we only use SSL secured channels to transmit them. We take ver seriously your security and your privacy therefore we comply with international security standards and only partner with certfied payment service providers. For more detailed info please drop us a line and we will be happy to answer your questions.');
}


/* Events Module 
 * --------------------------------------------------------------------
 */

function love(el) {
    var xid = el.id;
    var eid = xid.substring(5);
    var api = new $.RestClient();
    var email = $.cookie('charing_email');
    var data = {"data": {"eventid": eid, "islove": 1}};
    api.add('love', {url: 'user/love', stringifyData: true, email: email});
    var love = api.love.create(data);
    love.done(function(rs) {
        $("#love_" + eid).attr("onclick", "unlove(this)");
        $("#love_" + eid).html('<img src="images/heart-icon.png" alt="favorite" />');
        console.log('loved' + eid);
    });
}

function unloveFromFav(el) {
//alert('clicekd');
    unlove(el);
    //alert('done');
    $(":mobile-pagecontainer").pagecontainer("change", "#favorite", {allowSamePageTransition: true});
}

function unlove(el) {
    var xid = el.id;
    var eid = xid.substring(5);
    var api = new $.RestClient();
    var email = $.cookie('charing_email');
    var data = {"data": {"eventid": eid, "islove": 0}};
    api.add('love', {url: 'user/love', stringifyData: true, email: email});
    var love = api.love.create(data);
    love.done(function(rs) {
        $("#love_" + eid).attr("onclick", "love(this)");
        $("#love_" + eid).html('<img src="images/heart-icon-gray.png" alt="favorite" />');
        console.log('unloved' + eid);
    });
}

function single_event(id) {
    console.log('Single event' + id);
    var api = new $.RestClient();
    api.add('single_list');

    api.single_list.read(id).done(function(rs) {
        $("#event_item").loadTemplate($('#single_event_tpl'), rs.data.content, {append: false});
        $.cookie('event_share_img', rs.data.content[0].event_picture_small_link);
        console.log('before response s');
        calc_all();
        console.log('after response s');
    });
    console.log('after service call');
//		calc_all();
//    $(":mobile-pagecontainer").pagecontainer("change", "event.html&ui-page=single_event", {reload: true});
//    return false;
}

function comboup(el_id) {
    var new_sel = $('#' + el_id).prop("selectedIndex") - 1;
    if (new_sel >= 0) {
        $('#' + el_id).prop("selectedIndex", new_sel);
        $('#' + el_id).trigger('change');
    }
}

function combodown(el_id) {
    var new_sel = $('#' + el_id).prop("selectedIndex") + 1;
    var len = $('#' + el_id).find("option").length;
    if (new_sel < len) {
        $('#' + el_id).prop("selectedIndex", new_sel);
        $('#' + el_id).trigger('change');
    }
}

function changetype(el) {
    calc_all();
}

function plusone(input_id) {
    var val = parseInt($("#" + input_id).val());
    val = val + 1;
    if (val >= 100) {
        val = 99;
    }
    if (val < 10) {
        val = "0" + val;
    }
    $("#" + input_id).val(val);
    calc_all();

}

function minusone(input_id) {
    var val = parseInt($("#" + input_id).val());
    val = val - 1;
    if (val <= 0) {
        val = 1;
    }
    if (val < 10) {
        val = "0" + val;
    }
    $("#" + input_id).val(val);
    calc_all();
}

function calc_all() {
    var rate = parseFloat($('#ttype').val());
    var ticket = parseInt($('#ticket-quantity').val());
    var kp = parseInt($('#ticket-kp').val());
    var total = rate * ticket;
    var points = Math.ceil(rate * ticket * kp / 100);
    if (points < 1) {
        points = 1;
    }
    console.log(rate);
    $('#ticket-rate').html(rate.toFixed(2));
    $('#total-amt').html(total.toFixed(2));
    $('#points-amt').html(points);
    $('#ticket-count').html(ticket);
}

function user_exists(email) {
    // chk user details
    var api = new $.RestClient();
    var uid = $.base64.btoa(email);
    api.add('exists', {url: 'user/exists'});
    var exists = api.exists.read(uid);
    exists.done(function(rs) {
        val = rs.data.result;
        /*if (rs.data.result == true) {
         return true;
         } else {
         return false;
         } */
    });
    return true;
}

function booknow() {
    var amt = parseInt($('#total-amt').html());

    if (amt == 0) {
        console.log('free event: no card chk');
        confirmbook();
    } else {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        console.log('uid: ' + uid);
        var api = new $.RestClient();
        api.add('user', {url: 'user/details', stringifyData: true, email: email});
        var user = api.user.read();
        user.done(function(rs) {
            if (rs.data.charity_card_data !== "") {
                // $("#event_buy_confirm").popup("open");
                //alert("Your ticket was saved in 'Settings' in the 'My Tickets' folder.");
                confirmbook();
            } else {
                $(":mobile-pagecontainer").pagecontainer("change", "#payment");
            }
        });
    }
}

function submitcard() {

    Stripe.setPublishableKey('pk_test_JGaTl7MuzX79uvSKK6XdSatw');
    Stripe.card.createToken({
        number: $('#cardno').val(),
        cvc: $('#cardvcc').val(),
        exp_month: $('#cardexpmonth').val(),
        exp_year: $('#cardexpyear').val()
    }, stripeResponseHandler);
    return false;
}

function stripeResponseHandler(status, response) {
    if (response.error) {
        alert(response.error.message);
    } else {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('card', {url: 'user/registercard_enc/' + uid, stringifyData: true, email: email});
        var expmonth = $('#cardexpmonth').val();
        var expyear = $('#cardexpyear').val();
        var data = {"data": {
                "number": "****",
                "expiry": expmonth + "/" + expyear,
                "stripe": response.id
            }};
        var card = api.card.create(data);
        card.done(function(rs) {
            if (rs.data.result == true) {
                console.log('Card registerd');
                //$("#event_card_buy_confirm").popup("open");
                confirmbook();
            } else {
                alert('Card registration have been failed');
            }
        });
    }
}

function confirmbook() {
    var uid = $.cookie('charing_uid');
    var email = $.cookie('charing_email');
    var api = new $.RestClient();
    api.add('buy', {url: 'charity/buy/' + uid, stringifyData: true, email: email});

    var currency = $('#ticket-currencycode').val();
    if (currency === "pound") {
        currency = "GBP";
    }
    var data = {'data': {
            'event_id': $('#ticket-event-id').val(),
            'option': $('#ttype').find(":selected").text(),
            'currencycode': currency,
            'amount': $('#total-amt').html(),
            'quantity': parseInt($('#ticket-quantity').val().toString())
        }};
    var buy = api.buy.create(data);
    buy.done(function(rs) {
        if (rs.data.error) {
            alert(rs.data.error);
        } else {
            $.cookie('event_share_msg', rs.data.message);
            
            alert("Your ticket was saved in 'Settings' in the 'My Tickets' folder.");
            $(":mobile-pagecontainer").pagecontainer("change", "#share_event");
        }
    });
}

function updatecard() {
    alert('Your card details have been updated.');
    $(":mobile-pagecontainer").pagecontainer("change", "profile.html&ui-page=profile", {reloadPage: false});
    return false;
}


function single_shop(id) {
    var api = new $.RestClient();
    api.add('single_list');
    $.cookie('charing_current_eid', id);
    api.single_list.read(id).done(function(rs) {
        $("#shop_item").loadTemplate($('#single_shop_tpl'), rs.data.content, {append: false});
        $.cookie('shop_share_img', rs.data.content[0].event_picture_small_link);
        barcode_loader(id);
    });
}

function barcode_loader() {
    var email = $.cookie('charing_email');
    var eid = $.cookie('charing_current_eid');
    var api = new $.RestClient();
    api.add('barcode', {url: 'bazaar/transaction/new', email: email});
    var barcode = api.barcode.read();
    barcode.done(function(rs) {
        console.log('barcode_loader: ' + rs.data.transaction.barcode);
        console.log('barcode_loader: ' + rs.data.transaction.channel_id);
        $('#shopbarcode_' + eid).attr('src', 'https://api.charing.ringpay.net/v3/bazaar/barcode/300/' + rs.data.transaction.barcode + '.png');
        $('#barcode').html(rs.data.transaction.barcode);
        barcode_status(rs.data.transaction.channel_id, eid);
    });
}

function barcode_status(channel, eid) {
    var email = $.cookie('charing_email');
    var api = new $.RestClient();
    var dailog = "";
    var btimer = 90;
    var dotimer = true;
    looping = true;

    api.add('status', {url: 'bazaar/transaction/status/' + channel, email: email});
    (function() {

        var currentpage = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;
        if (dotimer)
            btimer = btimer - 1;
        if(currentpage != 'single_shop')
            looping = false;
        
        if (btimer == 0) {
            looping = false;
            setInterval(barcode_loader(), 1000);
        }
        ;

        $('#barcode_timer').html(btimer);
        api.status.read().done(function(rs) {
            if (typeof(rs.data.error) !== "undefined" && rs.data.error !== null) {
                if (rs.data.error == "barcode is invalid") {
                    console.log('barcode is invalid');
                    barcode_loader();
                } else {
                    console.log(rs.data.error);
                }
            }

            if (typeof(rs.data.transaction) !== "undefined" && rs.data.transaction !== null) {
                if (rs.data.transaction.transaction.status == "paired") {
                    console.log('Transaction paired');
                    dotimer = false;
                    if (dailog !== "paired") {
                        console.log('Transaction Id: ' + rs.data.transaction.transaction.transaction_id);
                        dailog = "paired";
                        var msg = "Please wait for " + rs.data.transaction.transaction.charity_name + " - " + rs.data.transaction.transaction.bazaar_name + " to insert info";
                        $.cookie("shop_buy_confirm_channel", channel);
                        $("#shop_buy_abort_msg").html(msg);
                        $("#shop_buy_abort").popup("open");
                    }

                } else if (rs.data.transaction.transaction.status == "price") {
                    console.log('Transaction Priced');
                    dotimer = false;
                    if (dailog !== "price") {
                        dailog = "price";
                        var msg = "Pay " + rs.data.transaction.transaction.total + " " + rs.data.transaction.transaction.currency + " to " + rs.data.transaction.transaction.charity_name + " - " + rs.data.transaction.transaction.bazaar_name;
                        $.cookie("shop_buy_confirm_channel", channel);
                        $("#shop_buy_confirm_msg").html(msg);
                        $("#shop_buy_abort").popup("close");
                        setTimeout(function() {
                            $("#shop_buy_confirm").popup("open");
                        }, 100);
                    }
                } else if (rs.data.transaction.transaction.status == "aborted") {
                    console.log('Transaction Aborted');
                    dotimer = false;
                    if (dailog !== "aborted") {
                        dailog = "aborted";
                        $("#shop_buy_abort").popup("close");
                        $("#shop_buy_confirm").popup("close");
                        setTimeout(function() {
                            $("#shop_buy_aborted").popup("open");
                        }, 100);
                    }
                    looping = false;
                }
                else if (rs.data.transaction.transaction.status == "payment_error") {
                    console.log('Transaction Payment Error');
                    looping = false;
                }

                else if (typeof(rs.data.message) !== "undefined" && rs.data.message !== null) {
                    console.log('Transaction Confirmed');
                    if (dailog !== "confirmed") {
                        dailog = "confirmed";
                        $("#shop_buy_confirmed").popup("open");
                    }
                    looping = false;
                }
            }

        });
        if (looping === true)
            setTimeout(arguments.callee, 1000);
    })();
}

function shop_transaction_abort() {
    var channel = $.cookie("shop_buy_confirm_channel");
    $.removeCookie("shop_buy_confirm_channel");
    var email = $.cookie('charing_email');
    var api = new $.RestClient();
    api.add('abort', {url: 'bazaar/transaction/abort', stringifyData: true, email: email});
    var data = {"data": {"channel_id": channel}};
    api.abort.create(data).done(function(rs) {
        console.log('shop_transaction_abort: ' + channel);
    });
}

function shopnow() {
    var uid = $.cookie('charing_uid');
    var email = $.cookie('charing_email');
    looping = false;
    var api = new $.RestClient();
    api.add('user', {url: 'user/details', stringifyData: true, email: email});
    var user = api.user.read();
    user.done(function(rs) {
        console.log('user response');
        if (rs.data.charity_card_data !== "") {
            shop_transaction_confirm();
        } else {
            $(":mobile-pagecontainer").pagecontainer("change", "#shop_payment");
        }
    });
}

function shop_submitcard() {
    Stripe.setPublishableKey('pk_test_JGaTl7MuzX79uvSKK6XdSatw');
    Stripe.card.createToken({
        number: $('#shopCardno').val(),
        cvc: $('#shopCardvcc').val(),
        exp_month: $('#shopCardexpmonth').val(),
        exp_year: $('#shopCardexpyear').val()
    }, shop_stripeResponseHandler);
    return false;
}

function shop_stripeResponseHandler(status, response) {
    if (response.error) {
        alert(response.error.message);
    } else {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('card', {url: 'user/registercard_enc/' + uid, stringifyData: true, email: email});
        var expmonth = $('#shopCardexpmonth').val();
        var expyear = $('#shopCardexpyear').val();
        var data = {"data": {
                "number": "****",
                "expiry": expmonth + "/" + expyear,
                "stripe": response.id
            }};
        var card = api.card.create(data);
        card.done(function(rs) {
            if (rs.data.result == true) {
                console.log('Card registerd');
                shop_transaction_confirm();
            } else {
                alert('Card registration have been failed');
            }
        });
    }
}

function shop_transaction_confirm() {
    var channel = $.cookie("shop_buy_confirm_channel");
    $.removeCookie("shop_buy_confirm_channel");
    var email = $.cookie('charing_email');
    var api = new $.RestClient();
    api.add('confirm', {url: 'bazaar/transaction/confirm', stringifyData: true, email: email});
    var data = {"data": {"channel_id": channel}};
    api.confirm.create(data).done(function(rs) {
        if (typeof(rs.info.error) !== "undefined" && rs.info.error !== "") {
            alert(rs.info.error);
        } else if (typeof(rs.data.error) !== "undefined" && rs.data.error !== "") {
            alert(rs.data.error);
        } else {
            $.cookie('shop_share_msg', rs.data.message);
            $(":mobile-pagecontainer").pagecontainer("change", "#share_shop");
        }
    });
}

function single_favorite(id) {
    var api = new $.RestClient();
    api.add('single_list');

    api.single_list.read(id).done(function(rs) {
//	alert(rs.data.content[0].is_event);
//	alert(rs.data.content[0].is_event == 1);
        if (rs.data.content[0].is_event == 1) {
            $(":mobile-pagecontainer").pagecontainer("change", "#single_event?id=" + id);
        } else {
            $(":mobile-pagecontainer").pagecontainer("change", "#single_shop?id=" + id);
        }
        //$("#favorite_item").loadTemplate($('#single_favorite_tpl'), rs.data.content, {append: false});
    });



}

function openMenu() {
}

function showProfile() {
    if ($.cookie('charing_uid')) {
        var uid = $.cookie('charing_uid');
        var api = new $.RestClient();
        var email = $.cookie('charing_email');
        api.add('user', {url: 'user/details', stringifyData: true, email: email});
        var user = api.user.read();
        user.done(function(rs) {
            $("#td-user-name").html(rs.data.charity_user_name);
            $("#td-user-email").html(rs.data.charity_user_email);
            $("#td-user-phone").html(rs.data.charity_user_phone);
        });
    }

}

function charingcoinsPage() {
    if ($.cookie('charing_uid')) {
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('coins', {url: 'user/karma', stringifyData: true, email: email});
        var coins = api.coins.read();
        coins.done(function(rs) {
            $("#charingcoins_coins").html(rs.data.total_karma);
        });
    } else {
        $(".charingcoins-info").html("You're not loged in");
    }
}


function showPurchase() {
//                $(document).on('pagebeforeshow', function() { 
    if ($.cookie('charing_uid')) {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');

        var api = new $.RestClient();
        api.add('purchases', {url: 'user/purchases', stringifyData: true, email: email});
        var purchases = api.purchases.read(uid);
        purchases.done(function(rs) {
            $("#purchases").empty();
            $.each(rs.data, function(a, b) {
                $("#purchases").loadTemplate($('#list_purchase_tpl'), b, {append: true});
            });
        });
    } else {
        $("#purchases").html("You're not loged in");
    }
//                });

}


function showHelp() {
    var api = new $.RestClient();
    var email = $.cookie('charing_email');
    api.add('help', {url: 'help', stringifyData: true, email: email});
    var help = api.help.read();
    help.done(function(rs) {
        $("#help_data").html(rs.data.en);
    });

}


function showMyticket() {
    if ($.cookie('charing_uid')) {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('myticket', {url: 'user/tickets', stringifyData: true, email: email});
        var mytickets = api.myticket.read(uid);
        mytickets.done(function(rs) {
            $("#mytickets").empty();

            $.each(rs.data, function(a, b) {
                var i = 1;
                for (i = 1; i <= b.ticket_q; i++) {
                    $("#mytickets").loadTemplate($('#list_myticket_tpl'), b, {append: true});
                }
            });
        });
    } else {
        $("#mytickets").html("You're not loged in");
    }

}


function showFavorites() {
    if ($.cookie('charing_uid')) {
        var api = new $.RestClient();
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        api.add('favorites', {url: 'list_loved', email: email});
        var favorites = api.favorites.read();
        favorites.done(function(rs) {
            $("#favorites").empty();
            $.each(rs.data.content, function(a, b) {
                $("#favorites").loadTemplate($('#list_favorite_tpl'), b, {append: true});
            });
        });
    } else {
        alert("You're not loged in");
        $(":mobile-pagecontainer").pagecontainer("load", "index.html");
        //$(":mobile-pagecontainer").pagecontainer("change", "index.html");
    }

}


function showEvents() {
    if ($.cookie('charing_uid')) {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('events', {url: 'list_event', email: email});
        var events = api.events.read();
        events.done(function(rs) {
            $("#events").empty();
            $.each(rs.data.content, function(a, b) {
                $("#events").loadTemplate($('#list_event_tpl'), b, {append: true});
            });
        });
    } else {
        $("#events").html("You're not loged in");
    }
}

function shareEvent() {
    var msg = $.cookie('event_share_msg');
    var img = $.cookie('event_share_img');
    $.removeCookie('event_share_img');
    $.removeCookie('event_share_msg');
    console.log('Values on share screen' + msg + " " + img);
    $('#share_event_img').attr('src', img);
    $('#share_event_msg').html(msg);
}

function showMshop() {
    if ($.cookie('charing_uid')) {
        var uid = $.cookie('charing_uid');
        var email = $.cookie('charing_email');
        var api = new $.RestClient();
        api.add('shops', {url: 'list_shop', email: email});
        var events = api.shops.read();
        events.done(function(rs) {
            $("#shops").empty();
            $.each(rs.data.content, function(a, b) {
                $("#shops").loadTemplate($('#list_shop_tpl'), b, {append: true});
            });
        });
    } else {
        alert("You're not loged in");
        $(":mobile-pagecontainer").pagecontainer("load", "index.html", {reloadPage: true});
        //$(":mobile-pagecontainer").pagecontainer("change", "index.html");
    }
}

function shareShop() {
    var msg = $.cookie('shop_share_msg');
    var img = $.cookie('shop_share_img');
    $.removeCookie('shop_share_img');
    $.removeCookie('shop_share_msg');
    $('#share_shop_img').attr('src', img);
    $('#share_shop_msg').html(msg);
}


function GooglePlusShare(url) {
    var fullurl = "https://plus.google.com/share?url=" + url;
    window.open(fullurl, '', "toolbar=0,location=0,height=450,width=550");
}

function FacebookShare(url) {
    var fullurl = "http://www.facebook.com/sharer/sharer.php?u=" + url;
    window.open(fullurl, '', "toolbar=0,location=0,height=450,width=650");
}

function TwitterShare(url, ttl) {
    var fullurl = "https://twitter.com/share?original_referer=http://www.charing.com/&source=tweetbutton&text=" + ttl +"&url=" + url;
    window.open(fullurl, '', "menubar=1,resizable=1,width=450,height=350");
}
