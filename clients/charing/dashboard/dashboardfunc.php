<?php



function favorites_for_all()  {
	$sql="SELECT cl.charity_name AS name, COUNT(1) AS count FROM `charity_favorites` cf 
JOIN charity_events ce ON cf.charity_id = ce.charity_event_id JOIN charity_list cl ON ce.charity_id = cl.charity_id GROUP BY cl.charity_id, cl.charity_name ORDER BY COUNT(1) DESC";
	
	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute();
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function purchases_for_all()  {
	$sql="SELECT seller as name, COUNT(`id`) AS count FROM `bazaar_transaction` WHERE UPPER(`status`) = 'PAID' GROUP BY seller ORDER BY COUNT(1) DESC";
	
	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute();
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function shares_for_all()  {
	$sql="SELECT cl.charity_name AS name, COUNT(1) AS count FROM `track_shares` ts 
JOIN charity_events ce ON ts.event_id = ce.charity_event_id JOIN charity_list cl ON ce.charity_id = cl.charity_id GROUP BY cl.charity_id, cl.charity_name ORDER BY COUNT(1) DESC";
	
	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute();
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function tickets_for_all()  {
	$sql="SELECT cl.charity_name as name,SUM(`ticket_q`) AS count FROM 
`charity_events_donations` ced
join `charity_events` ce on ce.charity_event_id = ced.charity_ticket_id join charity_list cl ON cl.charity_id = ce.charity_id
GROUP BY cl.charity_id, cl.charity_name ORDER BY SUM(ticket_q) DESC";
	
	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute();
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function favorite_by_id($eventid,$allow_empty=false)  {
	$sql="SELECT cl.charity_name as name,(DATE_FORMAT(`when`, '%d-%m-%Y-%H-%i')) AS date, COUNT(1) AS count FROM `charity_favorites` cf 
JOIN charity_events ce ON cf.charity_id = ce.charity_event_id JOIN charity_list cl ON ce.charity_id = cl.charity_id WHERE cl.charity_id = :charityId GROUP BY cl.charity_id, cl.charity_name, (DATE_FORMAT(`when`, '%d-%m-%Y-%H-%i')) ORDER BY `when`";		

	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute(array(':charityId' => $eventid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}


function shares_by_id($eventid,$allow_empty=false)  {
	$sql="SELECT cl.charity_name as name, (DATE_FORMAT(`quanto`, '%d-%m-%Y-%H-%i'))AS date, COUNT(1) AS count FROM `track_shares` ts 
JOIN charity_events ce ON ts.event_id = ce.charity_event_id JOIN charity_list cl ON ce.charity_id = cl.charity_id WHERE cl.charity_id = :charityId GROUP BY cl.charity_id, cl.charity_name, (DATE_FORMAT(`quanto`, '%d-%m-%Y-%H-%i'))ORDER BY `quanto` ASC";		

	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute(array(':charityId' => $eventid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}




function purchases_by_id($eventid,$allow_empty=false)  {
	$sql="SELECT seller as name, DATE_FORMAT(`updated_at`, '%d-%m-%Y-%H-%i') AS date, COUNT(`id`) AS count FROM `bazaar_transaction` WHERE UPPER(`status`) = 'PAID' AND seller = :sellerId GROUP BY seller,(DATE_FORMAT(`updated_at`, '%d-%m-%Y-%H-%i')) ORDER BY `updated_at` ASC";		

	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute(array(':sellerId' => $eventid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function tickets_by_id($eventid,$allow_empty=false)  {
	$sql="SELECT cl.charity_name as name, (DATE_FORMAT(FROM_UNIXTIME(processor_timestamp), '%d-%m-%Y-%H-%i')) AS date, SUM(`ticket_q`) AS count FROM `charity_events_donations` ced JOIN charity_events ce ON ced.charity_ticket_id = ce.charity_event_id JOIN charity_list cl ON ce.charity_id = cl.charity_id WHERE cl.charity_id = :charityId GROUP BY cl.charity_id, cl.charity_name, (DATE_FORMAT(FROM_UNIXTIME(processor_timestamp), '%d-%m-%Y-%H-%i')) ORDER BY processor_timestamp ASC";		

	$db = getDbConnection();
	$sth = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$sth->execute(array(':charityId' => $eventid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

/*
 * ===========================================================================================
 */

$app->get('/reports/favorites', function() use ($app){
	try {
		pretty_json_response(200, "/reports/favorites", Array("result"=>favorites_for_all()), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/shares', function() use ($app){
	try {
		pretty_json_response(200, "/reports/shares", Array("result"=>shares_for_all()), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/purchases', function() use ($app){
	try {
		pretty_json_response(200, "/reports/purchases", Array("result"=>purchases_for_all()), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/tickets', function() use ($app){
	try {
		pretty_json_response(200, "/reports/tickets", Array("result"=>tickets_for_all()), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/favorites/:eventid', function($eventid) use ($app){
	try {
		pretty_json_response(200, "/reports/favorites", Array("result"=>favorite_by_id($eventid)), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/shares/:eventid', function($eventid) use ($app){
	try {
		pretty_json_response(200, "/reports/shares", Array("result"=>shares_by_id($eventid)), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/purchases/:eventid', function($eventid) use ($app){
	try {
		pretty_json_response(200, "/reports/purchases", Array("result"=>purchases_by_id($eventid)), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});

$app->get('/reports/tickets/:eventid', function($eventid) use ($app){
	try {
		pretty_json_response(200, "/reports/tickets", Array("result"=>tickets_by_id($eventid)), false, null);		
	} catch(PDOException $e) {
		pretty_json_response(500, "PDO exception: ".$e->getMEssage(), $e->getTraceAsString(), false, null);
	}
});


?>
