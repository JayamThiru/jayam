<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'uravu');

define('ROW_CREATED_SUCCESSFULLY', 0);
define('ROW_CREATE_FAILED', 1);
define('ROW_ALREADY_EXISTED', 2);
define('ROW_UPDATED_SUCCESSFULLY', 3);
define('ROW_UPDATE_FAILED', 4);

?>
