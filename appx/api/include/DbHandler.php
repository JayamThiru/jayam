<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $mobile, $about, $image = "") {
        require_once 'PassHash.php';
        $response = array();
        $password = "123456";
        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(`name`, `email`, `password`, `mobile`, `about`, `image`, `api_key`, `status`) values(?, ?, ?, ?, ?, ?, ?, 1)");
            $stmt->bind_param("sssssss", $name, $email, $password, $mobile, $about, $image, $api_key);

            $result = $stmt->execute();


            if ($result) {
                $user_id = $stmt->insert_id;
                $sstmt = $this->conn->prepare("INSERT INTO user_location (user_id) values(?)");
                $sstmt->bind_param("i", $user_id);
                $sstmt->execute();
                $sstmt->close();
            }

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return ROW_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return ROW_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return ROW_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Updating user
     * @param Int $user_id User id
     * @param String $name User full name
     * @param String $tagline User tagline
     * @param String $image User image
     */
    public function updateUser($user_id, $name, $phone, $tagline, $image) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(name, email, `password`, phone, tagline, image, api_key, status) values(?, ?, ?, ?, ?, ?, ?, 1)");
            $stmt->bind_param("sssssss", $name, $email, $password, $phone, $tagline, $image, $api_key);

            $result = $stmt->execute();

            $stmt->close();
            if ($result) {
                return ROW_CREATED_SUCCESSFULLY;
            } else {
                return ROW_CREATE_FAILED;
            }
        } else {
            return ROW_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($db_password);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($db_password, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT id, name, email, api_key, status, created_at FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($id, $name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserByKey($api_key) {
        $stmt = $this->conn->prepare("SELECT u.id, l.lat, l.lng, l.last_update FROM users as u, user_location as l WHERE u.id = l.user_id and api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($id, $lat, $lng, $last);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["lat"] = $lat;
            $user["lng"] = $lng;
            $user["last"] = $last;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Fetching all users
     */
    public function getAllUsers($user, $dist = 100, $qry = "") {
        $_qry = "";
        $formula = " ((ACOS(SIN(" . $user['lat'] . " * PI() / 180) * SIN(lat * PI() / 180) + COS(" . $user['lat'] . " * PI() / 180) * COS(lat * PI() / 180) * COS((" . $user['lng'] . " - lng) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)";
        if ($qry !== "") {
            $_qry = " and u.about like ('%$qry%')";
        } 
        
        $query = "SELECT u.*, l.*, $formula as distance FROM users as u, user_location as l WHERE u.id = l.user_id $_qry and u.id !=  " . $user['id'] . " HAVING distance <= " . $dist ." ORDER BY distance ASC ";

        $result = $this->conn->query($query);
        
        return $result;
    }

    /**
     * Updating user
     * @param Int $user_id User id
     * @param String $lat User latitude
     * @param String $lng User longitide
     */
    public function updateLocation($user_id, $lat, $lng) {
        $response = array();

        // insert query
        $stmt = $this->conn->prepare("UPDATE user_location set lat = ?, `lng` = ? where user_id = ?");
        $stmt->bind_param("ddi", $lat, $lng, $user_id);

        $result = $stmt->execute();

        $stmt->close();

        if ($result) {
            return ROW_CREATED_SUCCESSFULLY;
        } else {
            return ROW_CREATE_FAILED;
        }

        return $response;
    }

    /* ------------- `events` table method ------------------ */

    /**
     * Creating new event
     * @param String $user_id user id to whom event belongs to
     * @param String $event event text
     */
    public function createEvent($user_id, $event) {
        $stmt = $this->conn->prepare("INSERT INTO events(event) VALUES(?)");
        $stmt->bind_param("s", $event);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // event row created
            // now assign the event to user
            $new_event_id = $this->conn->insert_id;
            $res = $this->createUserEvent($user_id, $new_event_id);
            if ($res) {
                // event created successfully
                return $new_event_id;
            } else {
                // event failed to create
                return NULL;
            }
        } else {
            // event failed to create
            return NULL;
        }
    }

    /**
     * Fetching single event
     * @param String $event_id id of the event
     */
    public function getEvent($event_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.event, t.status, t.created_at from events t, user_events ut WHERE t.id = ? AND ut.event_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $event_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $event, $status, $created_at);
            // TODO
            // $event = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["event"] = $event;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user events
     * @param String $user_id id of the user
     */
    public function getAllUserEvents($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM events t, user_events ut WHERE t.id = ut.event_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $events = $stmt->get_result();
        $stmt->close();
        return $events;
    }

    /**
     * Updating event
     * @param String $event_id id of the event
     * @param String $event event text
     * @param String $status event status
     */
    public function updateEvent($user_id, $event_id, $event, $status) {
        $stmt = $this->conn->prepare("UPDATE events t, user_events ut set t.event = ?, t.status = ? WHERE t.id = ? AND t.id = ut.event_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $event, $status, $event_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Deleting a event
     * @param String $event_id id of the event to delete
     */
    public function deleteEvent($user_id, $event_id) {
        $stmt = $this->conn->prepare("DELETE t FROM events t, user_events ut WHERE t.id = ? AND ut.event_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $event_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_events` table method ------------------ */

    /**
     * Function to assign a event to user
     * @param String $user_id id of the user
     * @param String $event_id id of the event
     */
    public function createUserEvent($user_id, $event_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_events(user_id, event_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $event_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }

}

?>
