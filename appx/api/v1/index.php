<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id, lat, lng from db - Global Variable
$user = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
    // print_r($headers);
    // Verifying Authorization Header
    if (isset($headers['Auth'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Auth'];

        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user;
            // get user primary key id
            $user = $db->getUserByKey($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /users
 * method - POST
 * params - name, email, password, phone, tagline
 */
$app->post('/users', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('name', 'email', 'mobile', 'about'));

            $response = array();

            // reading post params
            $name = $app->request->post('name');
            $email = $app->request->post('email');
            $mobile = $app->request->post('mobile');
            $about = $app->request->post('about');
            $image = $app->request->post('image');

            // validating email address
            validateEmail($email);

            // Handle image

            $db = new DbHandler();
            $res = $db->createUser($name, $email, $mobile, $about, $image);

            if ($res == ROW_CREATED_SUCCESSFULLY) {

                $user = $db->getUserByEmail($email);
                $response["error"] = false;
                if ($user != NULL) {
                    $response['id'] = $user['id'];
                    $response['name'] = $user['name'];
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['createdAt'] = $user['created_at'];
                }
                $response["message"] = "You are successfully registered";
            } else if ($res == ROW_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == ROW_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Update
 * url - /users
 * method - PUT
 * params - name, phone, tagline
 */
$app->put('/users/:id', 'authenticate', function($user_id) use($app) {
            // check for required params
            verifyRequiredParams(array('name', 'phone', 'tagline'));

            $response = array();

            // reading post params
            $name = $app->request->post('name');
            $phone = $app->request->post('phone');
            $tagline = $app->request->post('tagline');

            // validating email address
            validateEmail($email);

            // Handle image

            $db = new DbHandler();
            $res = $db->updateUser($user_id, $name, $phone, $tagline, $image);

            if ($res == ROW_UPDATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully updated";
            } else if ($res == ROW_UPDATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while updating";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - email
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email'));

            // reading post params
            $email = $app->request()->post('email');
            $password = "123456"; //$app->request()->post('password');
            $response = array();

            $db = new DbHandler();
            // check for correct email and password
            if ($db->checkLogin($email, $password)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['id'] = $user['id'];
                    $response['name'] = $user['name'];
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['createdAt'] = $user['created_at'];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });

/**
 * Listing all users
 * method GET
 * url /users          
 */
$app->get('/users(/:qry)', 'authenticate', function($qry = "") {
            global $user;

            $response = array();
            $db = new DbHandler();

            // fetching all user events
            $result = $db->getAllUsers($user, 100000, $qry);
            
            $response["error"] = false;
            $response["users"] = array();
            
            // looping through result and preparing events array
            while ($usr = $result->fetch_array(MYSQLI_ASSOC)) {
                $tmp = array();
                $tmp["id"] = $usr["id"];
                $tmp["name"] = $usr["name"];
                $tmp["email"] = $usr["email"];
                $tmp["mobile"] = $usr["mobile"];
                $tmp["about"] = $usr["about"];
                $tmp["image"] = $usr["image"] . "?width=100&height=100";
                $tmp["status"] = $usr["status"];
                $tmp["createdAt"] = $usr["created_at"];
                $tmp["distance"] = $usr["distance"];
                array_push($response["users"], $tmp);
            }

            echoRespnse(200, $response);
        });

/**
 * User Location
 * url - /loc
 * method - PUT
 * params - lat, lng
 */
$app->put('/loc/:uid', function($user_id) use ($app) {
            // check for required params
            verifyRequiredParams(array('lat', 'lng'));

            // reading post params
            $lat = $app->request()->post('lat');
            $lng = $app->request()->post('lng');
            $response = array();

            $db = new DbHandler();
            $res = $db->updateLocation($user_id, $lat, $lng);

            if ($res == ROW_UPDATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully updated";
            } else if ($res == ROW_UPDATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while updating";
            }

            echoRespnse(200, $response);
        });


/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/**
 * Listing all events of particual user
 * method GET
 * url /events          
 */
$app->get('/events', 'authenticate', function() {
            global $user;
            $response = array();
            $db = new DbHandler();

            // fetching all user events
            $result = $db->getAllUserEvents($user);

            $response["error"] = false;
            $response["events"] = array();

            // looping through result and preparing events array
            while ($event = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $event["id"];
                $tmp["event"] = $event["event"];
                $tmp["status"] = $event["status"];
                $tmp["createdAt"] = $event["created_at"];
                array_push($response["events"], $tmp);
            }

            echoRespnse(200, $response);
        });

/**
 * Listing single event of particual user
 * method GET
 * url /events/:id
 * Will return 404 if the event doesn't belongs to user
 */
$app->get('/events/:id', 'authenticate', function($event_id) {
            global $user;
            $response = array();
            $db = new DbHandler();

            // fetch event
            $result = $db->getEvent($event_id, $user);

            if ($result != NULL) {
                $response["error"] = false;
                $response["id"] = $result["id"];
                $response["event"] = $result["event"];
                $response["status"] = $result["status"];
                $response["createdAt"] = $result["created_at"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

/**
 * Creating new event in db
 * method POST
 * params - name
 * url - /events/
 */
$app->post('/events', 'authenticate', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('event'));

            $response = array();
            $event = $app->request->post('event');

            global $user;
            $db = new DbHandler();

            // creating new event
            $event_id = $db->createEvent($user, $event);

            if ($event_id != NULL) {
                $response["error"] = false;
                $response["message"] = "Event created successfully";
                $response["event_id"] = $event_id;
                echoRespnse(201, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create event. Please try again";
                echoRespnse(200, $response);
            }
        });

/**
 * Updating existing event
 * method PUT
 * params event, status
 * url - /events/:id
 */
$app->put('/events/:id', 'authenticate', function($event_id) use($app) {
            // check for required params
            verifyRequiredParams(array('event', 'status'));

            global $user;
            $event = $app->request->put('event');
            $status = $app->request->put('status');

            $db = new DbHandler();
            $response = array();

            // updating event
            $result = $db->updateEvent($user, $event_id, $event, $status);
            if ($result) {
                // event updated successfully
                $response["error"] = false;
                $response["message"] = "Event updated successfully";
            } else {
                // event failed to update
                $response["error"] = true;
                $response["message"] = "Event failed to update. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Deleting event. Users can delete only their events
 * method DELETE
 * url /events
 */
$app->delete('/events/:id', 'authenticate', function($event_id) use($app) {
            global $user;

            $db = new DbHandler();
            $response = array();
            $result = $db->deleteEvent($user, $event_id);
            if ($result) {
                // event deleted successfully
                $response["error"] = false;
                $response["message"] = "Event deleted succesfully";
            } else {
                // event failed to delete
                $response["error"] = true;
                $response["message"] = "Event failed to delete. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>