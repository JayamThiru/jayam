/**
 * 
 */
var http = require('http');

// push.js
// ========
module.exports = {
		push: function(msg,pushToken){
			console.log('entering');
			var bodyArgs =
			{
					  "request": 
			{'application' : "CD674-C9167",
			    'auth'      : "BYkEdHAOrXu6YTrmd6pQAHCCE06iQX7bmW5TjERGtzMaPsXefxNI6QWGQFcayypM3SEovhfDWsJZupJGYFCI",
			    'notifications':[{
			           'send_date' : 'now',
			           'content'   : msg.message,
			           'devices'   : [pushToken]
			     }]
			 }}

			var msgString = JSON.stringify(bodyArgs);
			console.log('sending'+msgString);

			var options = {
					  host: 'cp.pushwoosh.com',
					  port: 80,
					  path: '/json/1.3/createMessage',
					  method: 'POST',
					  headers:  {
							  'Content-Type': 'application/json',
							  'Content-Length': msgString.length,
							  'Access-Control-Allow-Origin':'*'
							}
					};
			// Setup the request.  The options parameter is
			// the object we defined above.
			var req = http.request(options, function(res) {
			  res.setEncoding('utf-8');


			  res.on('data', function(data) {
				  console.log('ondata');
				  console.log(data);
			  });

			  res.on('end', function() {
				  console.log('end');
			  });
			});

			req.on('error', function(e) {
				console.log('onerror');
				console.log(e);
			});

			req.write(msgString);

			req.end();
		}
};

