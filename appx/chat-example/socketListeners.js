/**
 * 
 */
var http = require('http');
var HashMap = require('hashmap').HashMap;
var pushNotif = require('./push');
var msgs = new HashMap();
var users = new HashMap();
var pushTokens = new HashMap();
// push.js
// ========
module.exports = function(io) {
	return {
		onChatMessage : function(msg, socket) {
			console.log("pushNotif.push(msg)r " + msg.message);


			users.forEach(function(value, key) {
				console.log("chat message user " + key + " : " + value);
			});

			sendChatMessageToReceiver(msg,io);

		},
		onSignIn : function(msg, socket) {
			console.log(' signing in ' + msg + ' socket.id ' + socket.id);
			users.set(msg, socket.id);
			msgs.forEach(function(value, key) {
				console.log(" user " + key + " : " + value);
			});
			if (msgs.has(msg)) {
				console.log('has messages' + msgs.get(msg).length);
				for (var i = 0; i < msgs.get(msg).length; i++) {
					console.log(msgs.get(msg)[i]);
					toSocket = io.sockets.connected[socket.id];
					if (toSocket) {
						console.log(' sending message ' + msgs.get(msg)[i]);
						if(msgs.get(msg)[i].type=='chat_message'){
							sendChatMessageToReceiver(msgs.get(msg)[i],io);
						}else{
							toSocket.emit(msgs.get(msg)[i].type, msgs.get(msg)[i]);
						}
					} else {
						console.log(' no route found for ' + msgs.get(msg)[i]);
					}

					// Do something
				}
				msgs.remove(msg);

				console.log(msgs.has(msg))
			}
		},
		onChatMessageStatusChange : function(msg, socket) {

			if (msg.from) {
				sendAckToSender(msg, 4,io);
			}
		},
		onChatMessageTyping : function(msg, socket) {
			if (msg.to) {

				toSocket = io.sockets.connected[msg.to];
				if (toSocket) {
					toSocket.emit('chat_message_typing', {
						from : msg.from,
						to : msg.to
					});
				} else {
					console.log(' no route found for ' + msg.to);
				}
				// Do something
			}
		},
		onPushToken : function(usr,socket) {
			console.log('on push Token');
			console.log(' push token received for ' + usr.email
					+ ' push token ' + usr.pushToken);
			pushTokens.set(usr.email, usr.pushToken);
			pushTokens.forEach(function(value, key) {
				console.log(" pushTokens " + key + " : " + value);
			});
		}

	};
};

function storeMsg(msg){
	console.log('Message stored'+msg);
}
function sendMsgToOfflineUser(msg,type, from, to){
	msg.from = from;
	msg.to  = to;
	msg.type = type;
	if (!msgs.has(to)) {

		msgs.set(to, []);
		console.log('setting new array '
				+ msgs.get(to));
	}

	msgs.get(to).push(msg);
	console.log(msgs.get(to));
	
}

function sendAckToSender(msg, newStatus,io){
	
	fromSocketId = users.get(msg.from);

	console.log(" user " + msg.from + " : " + fromSocketId);
	storeMsg(msg);
	if (fromSocketId) {
		fromSocket = io.sockets.connected[fromSocketId];
		var statusUpdateMsg= {
			id : msg.id,
			status : newStatus
		};
		if (fromSocket) {
			msg.status = newStatus;
			fromSocket.emit('chat_message_status_change', {
				id : msg.id,
				status : msg.status
			});
			storeMsg(msg);
		}else{
			sendMsgToOfflineUser(statusUpdateMsg,'chat_message_status_change',msg.to,msg.from);
		}
	}

}

function sendChatMessageToReceiver(msg,io){
	if (msg.to) {
		
		sendAckToSender(msg,2,io);
		
		toSocketId = users.get(msg.to);
		console.log(" user " + msg.to + " : " + toSocketId);
		if (toSocketId) {
			console.log("found tosocketId");
			toSocket = io.sockets.connected[toSocketId];
			if (toSocket) {
				console.log("found tosocket");
				msg.status = 3;
				toSocket.emit('chat_message', msg);
				storeMsg(msg);
				sendAckToSender(msg,3,io);
				return;
			}
		} 
		
		// tosocket not found
		{
			console.log("not found tosocketId");
			pushTokens.forEach(function(value, key) {
				console.log(" pushTokens '" + key + "' : " + value);
			});
			console.log(" pushTokens[msg.to] '" + msg.to + "' : " + pushTokens.get(msg.to));
			pushNotif.push(msg, pushTokens.get(msg.to));
			sendMsgToOfflineUser(msg,'chat_message',msg.from,msg.to);
		}

	}

}