var app = require('express')();
var svr = require('http').Server(app);
var io = require('socket.io')(svr);
var HashMap = require('hashmap').HashMap;
var db = require('./db');
var sockListener = require('./socketListeners');

var mySockListener = sockListener(io);

app.get('/', function(req, res){
  res.sendfile(__dirname + '/index.html');
});

	

function getClientsForChat(socketId){
	allClients = Object.keys(io.sockets.connected);
	var index = allClients.indexOf(socketId);
	if(allClients.length ==1){
		return [];
	}
	return allClients.splice(index-1,1);
}

io.on('connection', function(socket){
// 			socket.emit('chat message',{message: 'welcome '+socket.id+' Chat with '+getClientsForChat(socket.id), from: 'server', to: socket.id});
			//console.log(socket);
console.log(socket.id+' Others for chat '+getClientsForChat(socket.id));

//			socket.emit('chat message',{message: 'you can chat with '+getClientsForChat(socket.id), from: 'server', to: socket.id});
  socket.on('chat_message', function(msg){ mySockListener.onChatMessage(msg,socket)});
  
  socket.on('chat_message_status_change_email', function(msg){mySockListener.onChatMessageStatusChange(msg,socket)});

   socket.on('chat_message_typing', function(msg){mySockListener.onChatMessageTyping(msg,socket)});

  
   socket.on('pushToken', function(msg){mySockListener.onPushToken(msg,socket)});
  socket.on('signin', function(msg){mySockListener.onSignIn(msg,socket)});



  
});

svr.listen(3000, function(){
  console.log('listening on *:3000');
});


		
