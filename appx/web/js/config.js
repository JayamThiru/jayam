/**
 * Global config file to load server / application settings and constants 
 */
config = function() {
    'use strict';
    return {
        apiRoot: "http://localhost/uravu/v1/",
        fbAppId: "151948138332311",
        nodeJs: 'http://localhost:3000/'
    };
}();
