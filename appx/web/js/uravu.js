/* Global Config
 * --------------------------------------------------------
 */
var cookie_user = "Uravu_user_id";
var cookie_name = "Uravu_user_name";
var cookie_email = "Uravu_user_email";
var cookie_session = "Uravu_session_id";

var showlog = 3; // 0 = no log, 1 = error, 2 = error & warning, 3 = all

$(document).ready(function() {
    "use strict";
    var router = new $.mobile.Router([{
            "#login": {handler: "loginPage", events: "bs"},
            "#register": {handler: "registerPage", events: "bs"},
            "#user(?:[?/](.*))?": {handler: "userPage", events: "bs"},
            "#profile": {handler: "profilePage", events: "bs"},
            "#search(?:[?/](.*))?": {handler: "searchPage", events: "bs"},
            "#chat(?:[?/](.*))?": {handler: "chatPage", events: "bs"},
        }], {
        loginPage: function(type, match, ui) {
            log('Login page', 3);
        },
        registerPage: function(type, match, ui) {
            log('Register page', 3);
            register();
        },
        searchPage: function(type, match, ui) {
            log("Search page", 3);
            var qry = "";
            if (router.getParams(match[1]) !== null && router.getParams(match[1]) !== "") {
                console.log("Param: " + router.getParams(match[1]));
                var params = router.getParams(match[1]);
                qry = params.qry;
            }
            search(qry);
        },
        userPage: function(type, match, ui) {
            log("User page", 3);
            var params = router.getParams(match[1]);
            single_user(params.id);
        },
        profilePage: function(type, match, ui) {
            log("Profile page", 3);
            showProfile();
        },
        chatPage: function(type, match, ui) {
            log("Chat page", 3);
            var to = "";
            if (router.getParams(match[1]) !== null && router.getParams(match[1]) !== "") {
                to = router.getParams(match[1]).to;
            }
            console.log("To email is : " + to);
            showChat(to);
        }
    }, {
        ajaxApp: true,
        defaultHandler: function(type, ui, page) {
            log("Default handler called due to unknown route (" + type + ", " + ui + ", " + page + ")", 1);
        },
        defaultHandlerEvents: "s",
        defaultArgsRe: true
    });

    $.addTemplateFormatter({
        DateFormat: function(value, template) {
            if (template == "verylong") {
                return $.format.date(value, "ddd, dd/MM/yyyy HH:mm");
            } else if (template == "verylong2") {
                return $.format.date(value, "ddd, dd-MMM-yyyy HH:mm");
            } else if (template == "long") {
                return $.format.date(value, "dd/MM/yyyy HH:mm");
            } else if (template == "short-") {
                return $.format.date(value, "dd-MMM-yyyy");
            } else {
                return $.format.date(value, "dd/MM/yyyy");
            }
        },
        userId: function(value, options) {
            return "user_" + value;
        },
        userHref: function(value, options) {
            return "#user?id=" + value;
        },
        chatHref: function(value, options) {
            return "#chat?to=" + value;
        },
        userTitle: function(value, options) {
            return value;
        },
        distanceFormat: function(value, options) {
            return parseFloat(value).toFixed(4) + " mile";
        }
    });

    // update geo location in regular basis
    var watchID, geoLoc, lastLoc = {};
    lastLoc.lat = 0;
    lastLoc.lng = 0;
    (function() {
        if (navigator.geolocation) {
            // 60 seconds
            var options = {timeout: 60000};
            geoLoc = navigator.geolocation;
            watchID = geoLoc.watchPosition(updateLocation,
                    errorHandler,
                    options);
        } else {
            log("Sorry, browser does not support geolocation!", 1);
        }
    })();

    function updateLocation(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        if (lastLoc.lat !== lat || lastLoc.lng !== lng) {
            log("New location updated : " + lat + ", " + lng, 3);
            if ($.cookie(cookie_session)) {
                var uid = $.cookie(cookie_user);
                var data = {lat: lat, lng: lng};
                var api = new $.RestClient();
                api.add('loc', {url: 'loc/' + uid});
                var loc = api.loc.update(data);
                loc.done(function(rs) {
                    lastLoc.lat = lat;
                    lastLoc.lng = lng;
                    log("Updated location", 3);
                });
            }
        } else {
            // log('No change in location', 3);
        }
    }

    function errorHandler(err) {
        if (err.code == 1) {
            log("Access is denied!", 1);
        } else if (err.code == 2) {
            log("Position is unavailable!", 1);
        }
    }

});

/* General functions
 * -------------------------------------------------------------------
 */
function log(msg, level) {
    if(typeof(level) === "undefined") {
        level = 3;
    }
    var logname = {0: "Disabled", 1: "Error", 2: "Warning", 3: "Info"};
    if (level <= showlog) {
        console.log(logname[level] + ": " + msg);
    }
}

/* Login Module
 * --------------------------------------------------------------------
 */

function login() {
    openFB.login(
            function(response) {
                if (response.status === 'connected') {
                    log('Facebook login succeeded, got access token: ' + response.authResponse.token, 3);
                    openFB.api({
                        path: '/me',
                        success: function(data) {
                            log('FB Login with ' + data.email, 3);
                            var api = new $.RestClient();
                            api.add('user', {url: 'login', stringifyData: false});
                            var user = api.user.create({email: data.email});
                            user.done(function(rs) {
                                if (rs.error === false) {
                                    log('Already user exists', 3);
                                    $.cookie(cookie_user, rs.id);
                                    $.cookie(cookie_email, data.email);
                                    $.cookie(cookie_name, rs.name);
                                    $.cookie(cookie_session, rs.apiKey);
                                    $(":mobile-pagecontainer").pagecontainer("change", "#search");
                                } else {
                                    log('New user', 3);
                                    $(":mobile-pagecontainer").pagecontainer("change", "#register");
                                }
                            });
                        },
                        error: errorHandler});
                } else {
                    log('Facebook login failed: ' + response.error, 1);
                }
            }, {scope: 'email,read_stream,publish_stream'});
}

function getInfo() {
    openFB.api({
        path: '/me',
        success: function(data) {
            console.log(JSON.stringify(data));
        },
        error: errorHandler});
}

function share() {
    openFB.api({
        method: 'POST',
        path: '/me/feed',
        params: {
            message: 'Hey! I installed Uravu App'
        },
        success: function() {
            alert('the item was posted on Facebook');
        },
        error: errorHandler});
}

function logout() {
    openFB.logout(
            function() {
                alert('Logout successful');
            },
            errorHandler);
    $.removeCookie(cookie_user);
    $.removeCookie(cookie_email);
    $.removeCookie(cookie_name);
    $.removeCookie(cookie_session);
    $(":mobile-pagecontainer").pagecontainer("change", "#login", {reload: true});
}

function errorHandler(error) {
    log("FB login: " + error.message, 1);
}

function register() {
    openFB.api({
        path: '/me',
        success: function(data) {
            console.log(data);
            log('FB register with ' + data.email, 3);
            $("#name").val(data.first_name);
            $("#email").val(data.email);
            $("#about").val(data.bio);
            $("#reg_pro_image").attr("src", "https://graph.facebook.com/" + data.id + "/picture");
        },
        error: errorHandler});
    return false;
}

function saveRegister() {
    if (validateRegistrer()) {
        var name = $("#name").val(),
                email = $("#email").val(),
                about = $("#about").val(),
                mobile = $("#mobile").val(),
                image = $("#reg_pro_image").attr("src");

        var data = {name: name, email: email, about: about, mobile: mobile, image: image};
        var api = new $.RestClient();
        api.add('reg', {url: 'users'});
        var reg = api.reg.create(data);
        reg.done(function(rs) {
            if (rs.error === false) {
                $.cookie(cookie_user, rs.id);
                $.cookie(cookie_email, email);
                $.cookie(cookie_session, rs.apiKey);
                $(":mobile-pagecontainer").pagecontainer("change", "#search");
            } else {
                log('New user', 3);
                $(":mobile-pagecontainer").pagecontainer("change", "#register");
            }
        });


        $(":mobile-pagecontainer").pagecontainer("change", "#search");
    }
    return false;
}

function validateRegistrer() {
    if (!validateEmail(jQuery("#email").val())) {
        alert("Please enter valid email");
        return false;
    }
    if ($.trim($("#name").val()).length < 6) {
        alert("Name at least 6 char");
        return false;
    }
    if ($.trim($("#about").val()).length < 25) {
        alert("Tell about you at least with 25 char");
        return false;
    }
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function showProfile() {
    if ($.cookie(cookie_session)) {
        var uid = $.cookie('charing_uid');
        var api = new $.RestClient();
        var email = $.cookie('charing_email');
        api.add('user', {url: 'user/details', stringifyData: true, email: email});
        var user = api.user.read();
        user.done(function(rs) {
            $("#pro-user-name").val(rs.data.charity_user_name);
            $("#pro-user-email").val(rs.data.charity_user_email);
            $("#pro-user-phone").val(rs.data.charity_user_phone);
        });
    }
}

function saveProfile() {
    var name = $("#pro-user-name").val();
    var phone = $("#pro-user-phone").val();
    var uid = $.cookie('charing_uid');
    var email = $.cookie('charing_email');
    var api = new $.RestClient();
    var data = {"data":
                {
                    "name": name,
                    "email": email,
                    "phone": phone,
                    "picture": "",
                    "socialname": "",
                    "socialdata": ""
                }
    };
    api.add('update', {url: 'user/' + uid, stringifyData: true, email: email});
    var update = api.update.create(data);
    update.done(function(rs) {
        if (rs.data.result == true) {
            var uid = $.base64.btoa(email);
            log('Profile updated', 2);
            showProfile();
        }
    });
}

function search(qry) {
    if ($.cookie(cookie_session)) {
        var query = "";
        qry = $.trim(qry);
        if (qry !== "") {
            query = "/" + qry;
            $("#query").val(qry);
        }
        var api = new $.RestClient();

        api.add('users', {url: 'users' + query, apiKey: $.cookie(cookie_session)});

        var users = api.users.read();
        users.done(function(rs) {
            $("#users").empty();
            $.each(rs.users, function(a, b) {
                $("#users").loadTemplate($('#users_list_tpl'), b, {append: true});
            });
        });
    }
    return false;
}

function submitSearch() {
    var qry = $("#query").val();
    qry = $.trim(qry);
    if (qry.length < 4) {
        alert("please enter at leaset 4 char");
    } else {
        search(qry);
    }
    return false;
}

function showChat(to) {
    log("I'm at showChat() for " + to, 3);
    var from = $.cookie(cookie_email);
    if (to === "") {
        log("To email missing can't initiate chat", 1);
        alert("To mail id is missing");
        return false;
    }

    // Create Div if not created
    var to_id = "chat_" + to.toLowerCase();
    to_id = to_id.replace(/\@/g, "_");
    to_id = to_id.replace(/\./g, "_");
    current_chat_window = to_id;
    if ($("#" + to_id).length == 0) {
        log("New chat window opened " + to_id, 3);
        $("#messages").append('<div id="' + to_id + '" class="chat-window"></div>');
    }
    $('div.chat-window').hide(300, function() {
        $('#' + to_id + '.chat-window').show();
        log("Showing chat window for " + to_id, 3);
    });
    $("#to").val(to);
    return true;
}

function showHelp() {
    var api = new $.RestClient();
    var email = $.cookie('charing_email');
    api.add('help', {url: 'help', stringifyData: true, email: email});
    var help = api.help.read();
    help.done(function(rs) {
        $("#help_data").html(rs.data);
    });
}

function socialShare(social, url, ttl, desc) {
    var surl = webRoot + url;

    if (social == 'facebook') {
        var fullurl = "http://www.facebook.com/sharer/sharer.php?u=" + surl;
        window.open(fullurl, '', "toolbar=0,location=0,height=450,width=650");
    } else if (social == 'gplus') {
        var fullurl = "https://plus.google.com/share?url=" + surl;
        window.open(fullurl, '', "toolbar=0,location=0,height=450,width=550");
    } else if (social == 'twitter') {
        var fullurl = "https://twitter.com/share?original_referer=http://www.charing.com/&source=tweetbutton&text=" + ttl + "&url=" + surl;
        window.open(fullurl, '', "menubar=1,resizable=1,width=450,height=350");
    }
}
